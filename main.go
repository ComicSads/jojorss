package main

import (
	"bufio"
	"github.com/mmcdole/gofeed"
	"log"
	"os"
	"path/filepath"
	"strings"
)

/* TODO
- add documentation
- add comments
- getAlertFile() and getLogFile() do basically the same thing
- getDataDir() and getDesktopDir() do basically the same thing
*/

const rssfeed = "https://hiwamatanoboru.com/feed/"
const currentPart = "JoJolion"
const logfile = "JoJoRSS"

func main() {
	fp := gofeed.NewParser()
	feed, err := fp.ParseURL(rssfeed)
	if err != nil {
		log.Fatalf("Error while doing internet things: %s", err)
	}
	for _, article := range feed.Items {
		if !(strings.HasPrefix(article.Title, currentPart)) {
			continue
		} else if strings.Contains(article.Title, "Volume") {
			continue
		}
		splitTitle := strings.Split(article.Title, " ")
		chapterCount := splitTitle[len(splitTitle)-1]
		if checkLogged(chapterCount) {
			os.Exit(0) // Already recorded in log
		}
		writeChapter(chapterCount)
		alertFile(chapterCount)
	}
}

func alertFile(chapter string) {
	appendToFile(getAlertFile(), "Chapter "+chapter)
}

func writeChapter(chapter string) {
	appendToFile(getLogFile(), currentPart+chapter)
}

func appendToFile(path string, write string) {
	file, err := os.OpenFile(path, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatalf("Cannot open %s for writing: %s", path, err)
	}
	defer file.Close()
	if _, err := file.WriteString(write + "\n"); err != nil {
		log.Fatalf("Cannot write to %s: %s", path, err)
	}
}

func checkLogged(chapter string) bool {
	file, err := os.Open(getLogFile())
	if err != nil {
		if os.IsNotExist(err) {
			return false
		}
		log.Fatalf("Error opening logfile '%s': %s")
	}
	defer file.Close()
	ret := false

	sc := bufio.NewScanner(file)
	for sc.Scan() {
		if sc.Text() == currentPart+chapter {
			ret = true
		}
	}
	if err := sc.Err(); err != nil {
		log.Fatalf("I don't know what this is I copied this code from stack overfow: %s", err)
	}
	return ret
}
func getLogFile() string {
	return filepath.Join(getDataDir(), logfile)
}

func getAlertFile() string {
	return filepath.Join(getDesktopDir(), "There's a new "+currentPart+" chapter out!")
}
func getDesktopDir() string {
	var path string
	if dir := os.Getenv("XDG_DESKTOP_DIR"); dir != "" {
		path = filepath.Clean(dir)
	} else if home := os.Getenv("HOME"); home != "" {
		path = filepath.Join(home, "Desktop")
	} else {
		path = "."
	}
	return path
}

func getDataDir() string {
	var path string
	if dir := os.Getenv("XDG_DATA_HOME"); dir != "" {
		path = filepath.Clean(dir)
	} else if home := os.Getenv("HOME"); home != "" {
		path = filepath.Join(home, ".local", "share")
	} else {
		path = "."
	}
	return path
}
